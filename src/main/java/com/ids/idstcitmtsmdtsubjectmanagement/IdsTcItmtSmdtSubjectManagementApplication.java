package com.ids.idstcitmtsmdtsubjectmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
// todo: Incluir el manejo de excepciones
// todo: Añadir el resto de métodos
// todo: Seguir las especificaciones del swagger al pie de la letra
// todo: Cambiar el método patch por el método put
// TODO: Investigar sobre la diferencia entre persisit (genera nuevos objetos) y merge

@SpringBootApplication
public class IdsTcItmtSmdtSubjectManagementApplication {

  public static void main(String[] args) {
    SpringApplication.run(IdsTcItmtSmdtSubjectManagementApplication.class, args);
  }
}
