package com.ids.idstcitmtsmdtsubjectmanagement.exception;

/**
 * Exception thrown when the requested resource is not found.
 */
public class ResourceNotFoundException extends RuntimeException {

  /**
   * Class constructor to set the exception message.
   *
   * @param message exception message
   */
  public ResourceNotFoundException(String message) {
    super(message);
  }
}
