package com.ids.idstcitmtsmdtsubjectmanagement.exception;

/**
 * Exception thrown when the input parameter is invalid.
 */
public class InvalidParameterException extends RuntimeException {

  /**
   * Class constructor to set the exception message.
   *
   * @param message exception message
   */
  public InvalidParameterException(String message) {
    super(message);
  }
}
