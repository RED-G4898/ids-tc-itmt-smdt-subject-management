package com.ids.idstcitmtsmdtsubjectmanagement.exception;

/**
 * Exception thrown when the user is not authorized to perform the requested operation.
 */
public class UnauthorizedException extends RuntimeException {

  /**
   * Class constructor to set the exception message.
   *
   * @param message exception message
   */
  public UnauthorizedException(String message) {
    super(message);
  }
}
