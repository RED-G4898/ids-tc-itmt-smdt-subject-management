package com.ids.idstcitmtsmdtsubjectmanagement.exception;

/**
 * Exception thrown when general exception occurs.
 */
public class Exception extends RuntimeException {

  /**
   * Class constructor to set the exception message.
   *
   * @param message exception message
   */
  public Exception(String message) {
    super(message);
  }
}