package com.ids.idstcitmtsmdtsubjectmanagement.controller;

import com.ids.idstcitmtsmdtsubjectmanagement.exception.AccessDeniedException;
import com.ids.idstcitmtsmdtsubjectmanagement.exception.InvalidParameterException;
import com.ids.idstcitmtsmdtsubjectmanagement.exception.ResourceNotFoundException;
import com.ids.idstcitmtsmdtsubjectmanagement.exception.UnauthorizedException;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.ErrorResponse;
import com.ids.idstcitmtsmdtsubjectmanagement.util.ExceptionDefaultValues;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.time.LocalDateTime;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Advice controller to handle global controller exceptions.
 */
@RestControllerAdvice
public class ExceptionHandlerController {

  /**
   * Handle invalid parameter exception.
   *
   * @param ex Exception to handle
   * @return ResponseEntity&lt;ErrorResponse&gt;
   */
  @ExceptionHandler(InvalidParameterException.class)
  protected ResponseEntity<ErrorResponse> handleInvalidParameter(InvalidParameterException ex) {
    return buildErrorResponse(HttpStatus.BAD_REQUEST,
        ExceptionDefaultValues.INVALID_PARAMETER_EXCEPTION_MESSAGE,
        ex.getMessage());
  }

  /**
   * Handle unauthorized exception.
   *
   * @param ex Exception to handle
   * @return ResponseEntity&lt;ErrorResponse&gt;
   */
  @ExceptionHandler(UnauthorizedException.class)
  protected ResponseEntity<ErrorResponse> handleUnauthorized(UnauthorizedException ex) {
    return buildErrorResponse(HttpStatus.UNAUTHORIZED,
        ExceptionDefaultValues.UNAUTHORIZED_EXCEPTION_MESSAGE,
        ex.getMessage());
  }

  /**
   * Handle access denied exception.
   *
   * @param ex Exception to handle
   * @return ResponseEntity&lt;ErrorResponse&gt;
   */
  @ExceptionHandler(AccessDeniedException.class)
  protected ResponseEntity<ErrorResponse> handleAccessDenied(AccessDeniedException ex) {
    return buildErrorResponse(HttpStatus.FORBIDDEN,
        ExceptionDefaultValues.ACCESS_DENIED_EXCEPTION_MESSAGE,
        ex.getMessage());
  }

  /**
   * Handle resource not found exception.
   *
   * @param ex Exception to handle
   * @return ResponseEntity&lt;ErrorResponse&gt;
   */
  @ExceptionHandler(ResourceNotFoundException.class)
  protected ResponseEntity<ErrorResponse> handleResourceNotFound(ResourceNotFoundException ex) {
    return buildErrorResponse(HttpStatus.NOT_FOUND,
        ExceptionDefaultValues.RESOURCE_NOT_FOUND_EXCEPTION_MESSAGE,
        ex.getMessage());
  }

  /**
   * Handle no handler found exception.
   *
   * @param ex Exception to handle
   * @return ResponseEntity&lt;ErrorResponse&gt;
   */
  @ExceptionHandler(NoHandlerFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  protected ResponseEntity<ErrorResponse> handleNoHandlerFound(NoHandlerFoundException ex) {
    return buildErrorResponse(HttpStatus.NOT_FOUND,
        ExceptionDefaultValues.RESOURCE_NOT_FOUND_EXCEPTION_MESSAGE,
        ex.getMessage());
  }

  /**
   * Handle method argument not valid exception.
   *
   * @param ex Exception to handle
   * @return ResponseEntity&lt;ErrorResponse&gt;
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  protected ResponseEntity<ErrorResponse> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex) {
    String details = ex.getBindingResult().getAllErrors().stream()
        .map(objectError -> {
          if (objectError instanceof FieldError) {
            FieldError fieldError = (FieldError) objectError;
            return fieldError.getField() + " " + fieldError.getDefaultMessage();
          } else {
            return objectError.getDefaultMessage();
          }
        })
        .collect(Collectors.joining(", "));
    return buildErrorResponse(HttpStatus.BAD_REQUEST,
        ExceptionDefaultValues.INVALID_PARAMETER_EXCEPTION_MESSAGE, details);
  }

  /**
   * Handle general exceptions.
   *
   * @param ex Exception to handle
   * @return ResponseEntity&lt;ErrorResponse&gt;
   */
  @ExceptionHandler(Exception.class)
  protected ResponseEntity<ErrorResponse> handleException(Exception ex) {
    return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR,
        ExceptionDefaultValues.EXCEPTION_MESSAGE, ex.getMessage());
  }

  /**
   * Build error responses.
   *
   * @param status  Http status
   * @param code    Http status code
   * @param details Error details
   * @return ResponseEntity&lt;ErrorResponse&gt;
   */
  private ResponseEntity<ErrorResponse> buildErrorResponse(HttpStatus status, String code,
      String details) {
    return ResponseEntity.status(status).body(
        ErrorResponse.builder().type(status.getReasonPhrase()).code(code).details(details).uuid(
            UUID.randomUUID().toString()).timestamp(LocalDateTime.now()).build());
  }
}
