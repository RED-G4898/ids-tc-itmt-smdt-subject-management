package com.ids.idstcitmtsmdtsubjectmanagement.controller;

import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectCreateRequest;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectMutationResponse;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectQueryResponse;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectUpdateRequest;
import com.ids.idstcitmtsmdtsubjectmanagement.service.SubjectService;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.validation.Valid;

/**
 * Controller for subject related API requests.
 */
@RestController
@RequestMapping("${api.basePath}")
@Validated // Prepara al controlador para realizar validaciones
public class SubjectController {

  private final SubjectService subjectService;

  public SubjectController(SubjectService subjectService) {
    this.subjectService = subjectService;
  }

  /**
   * Get method to retrieve a subject by id.
   *
   * @param subjectId id of the subject to retrieve
   * @return ResponseEntity&lt;SubjectQueryResponse&gt;
   */
  @GetMapping(value = "${api.path.id}")
  public ResponseEntity<SubjectQueryResponse> getById(@PathVariable String subjectId) {
    return ResponseEntity.ok(subjectService.getSubjectById(subjectId));
  }

  /**
   * Get method to retrieve a subject by name.
   *
   * @param name name of the subject to retrieve
   * @return ResponseEntity&lt;SubjectQueryResponse&gt;
   */
  @GetMapping(value = "${api.path.name}")
  public ResponseEntity<SubjectQueryResponse> getByName(@PathVariable String name) {
    return ResponseEntity.ok(subjectService.getSubjectByName(name));
  }

  /**
   * Get method to retrieve a subject by abbreviation.
   *
   * @param abbreviation abbreviation of the subject to retrieve
   * @return ResponseEntity&lt;SubjectQueryResponse&gt;
   */
  @GetMapping(value = "${api.path.abbreviation}")
  public ResponseEntity<SubjectQueryResponse> getByAbbreviation(@PathVariable String abbreviation) {
    return ResponseEntity.ok(subjectService.getSubjectByAbbreviation(abbreviation));
  }

  /**
   * Get method to retrieve all subjects in an array.
   *
   * @return ResponseEntity&lt;List &lt; SubjectQueryResponse&gt;&gt;
   */
  @GetMapping(value = "${api.path}")
  public ResponseEntity<List<SubjectQueryResponse>> getAll() {
    return ResponseEntity.ok(subjectService.getAllSubjects());
  }

  /**
   * Post method to create a new subject.
   *
   * @param subjectCreateRequest request body with the new subject data
   * @return ResponseEntity&lt;SubjectMutationResponse&gt;
   */
  @PostMapping(value = "${api.path}")
  public ResponseEntity<SubjectMutationResponse> create(
      @Valid @RequestBody SubjectCreateRequest subjectCreateRequest) {
    return ResponseEntity.ok(subjectService.createSubject(subjectCreateRequest));
  }

  /**
   * Put method to update a subject.
   *
   * @param subjectId            id of the subject to update
   * @param subjectUpdateRequest request body with the subject data to update
   * @return ResponseEntity&lt;SubjectMutationResponse&gt;
   */
  @PutMapping(value = "${api.path.id}")
  public ResponseEntity<SubjectMutationResponse> update(@PathVariable String subjectId,
      @RequestBody SubjectUpdateRequest subjectUpdateRequest) {
    return ResponseEntity.ok(subjectService.updateSubject(subjectId, subjectUpdateRequest));
  }

  /**
   * Delete method to change a subject status to 0 in order to deactivate de subject by id.
   *
   * @param subjectId id of the subject to deactivate
   * @return ResponseEntity&lt;Void&gt;
   */
  @DeleteMapping(value = "${api.path.id}")
  public ResponseEntity<Void> delete(@PathVariable String subjectId) {
    subjectService.deleteSubject(subjectId);
    return ResponseEntity.ok().build();
  }
}

