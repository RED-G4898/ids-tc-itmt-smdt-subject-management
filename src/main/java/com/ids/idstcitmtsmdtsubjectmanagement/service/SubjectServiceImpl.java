package com.ids.idstcitmtsmdtsubjectmanagement.service;

import static com.ids.idstcitmtsmdtsubjectmanagement.util.SubjectDefaultValues.SUBJECT_NOT_FOUND;
import static com.ids.idstcitmtsmdtsubjectmanagement.util.SubjectDefaultValues.SUBJECT_STATUS_INACTIVE;

import com.ids.idstcitmtsmdtsubjectmanagement.exception.ResourceNotFoundException;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectCreateRequest;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectMutationResponse;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectQueryResponse;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectUpdateRequest;
import com.ids.idstcitmtsmdtsubjectmanagement.model.entity.Subject;
import com.ids.idstcitmtsmdtsubjectmanagement.model.repository.SubjectRepository;
import com.ids.idstcitmtsmdtsubjectmanagement.util.SubjectAdapter;
import com.ids.idstcitmtsmdtsubjectmanagement.util.SubjectUtils;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service implementation for Subject related operations.
 */
@Service
public class SubjectServiceImpl implements SubjectService {

  private final SubjectRepository subjectRepository;
  private final SubjectAdapter subjectAdapter;
  private final SubjectUtils subjectUtils;

  /**
   * Constructor.
   *
   * @param subjectRepository Subject repository.
   * @param subjectAdapter Subject adapter.
   * @param subjectUtils Subject utils.
   */
  public SubjectServiceImpl(SubjectRepository subjectRepository, SubjectAdapter subjectAdapter,
      SubjectUtils subjectUtils) {
    this.subjectRepository = subjectRepository;
    this.subjectAdapter = subjectAdapter;
    this.subjectUtils = subjectUtils;
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public SubjectQueryResponse getSubjectById(String id) {
    return subjectRepository.findById(id).map(subjectAdapter::toSubjectQueryResponse)
        .orElseThrow(() -> new ResourceNotFoundException(SUBJECT_NOT_FOUND));
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public SubjectQueryResponse getSubjectByName(String name) {
    return subjectRepository.findByName(name).map(subjectAdapter::toSubjectQueryResponse)
        .orElseThrow(() -> new ResourceNotFoundException(SUBJECT_NOT_FOUND));
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public SubjectQueryResponse getSubjectByAbbreviation(String abbreviation) {
    return subjectRepository.findByAbbreviation(abbreviation)
        .map(subjectAdapter::toSubjectQueryResponse)
        .orElseThrow(() -> new ResourceNotFoundException(SUBJECT_NOT_FOUND));
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public List<SubjectQueryResponse> getAllSubjects() {
    return subjectRepository.findAll().stream().map(subjectAdapter::toSubjectQueryResponse).collect(
        Collectors.toList());
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public SubjectMutationResponse createSubject(SubjectCreateRequest subjectCreateRequest) {
    subjectUtils.validateCreateSubject(subjectAdapter.toSubject(subjectCreateRequest));
    return subjectAdapter.toSubjectMutationResponse(
        subjectRepository.save(subjectAdapter.toSubject(subjectCreateRequest)));
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public SubjectMutationResponse updateSubject(String id,
      SubjectUpdateRequest subjectUpdateRequest) {
    Subject subject = subjectRepository.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException(SUBJECT_NOT_FOUND));
    subjectUtils.validateUpdateSubject(subjectUpdateRequest);
    return subjectAdapter.toSubjectMutationResponse(
        subjectRepository.save(subjectAdapter.toSubject(subject, subjectUpdateRequest)));
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public void deleteSubject(String id) {
    subjectRepository.findById(id).map(subject -> subjectRepository.save(
            subjectAdapter.toSubject(SUBJECT_STATUS_INACTIVE, subject)))
        .orElseThrow(() -> new ResourceNotFoundException(SUBJECT_NOT_FOUND));
  }
}
