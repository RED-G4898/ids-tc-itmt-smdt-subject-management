package com.ids.idstcitmtsmdtsubjectmanagement.service;

import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectCreateRequest;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectMutationResponse;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectQueryResponse;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectUpdateRequest;

import java.util.List;

public interface SubjectService {

  /**
   * Get method to retrieve a subject by id.
   *
   * @param id id of the subject to find
   * @return SubjectQueryResponse
   */
  SubjectQueryResponse getSubjectById(String id);

  /**
   * Get method to retrieve a subject by name.
   *
   * @param name name of the subject to find
   * @return SubjectQueryResponse
   */
  SubjectQueryResponse getSubjectByName(String name);

  /**
   * Get method to retrieve a subject by abbreviation.
   *
   * @param abbreviation abbreviation of the subject to find
   * @return SubjectQueryResponse
   */
  SubjectQueryResponse getSubjectByAbbreviation(String abbreviation);

  /**
   * Get method to retrieve all subjects.
   *
   * @return List&lt;SubjectQueryResponse&gt;
   */
  List<SubjectQueryResponse> getAllSubjects();

  /**
   * Post method to create a new subject.
   *
   * @param subjectCreateRequest request body with the new subject data
   * @return SubjectMutationResponse
   */
  SubjectMutationResponse createSubject(SubjectCreateRequest subjectCreateRequest);

  /**
   * Put method to update a subject.
   *
   * @param id                   id of the subject to update
   * @param subjectUpdateRequest request body with the subject data to update
   * @return SubjectMutationResponse
   */
  SubjectMutationResponse updateSubject(String id, SubjectUpdateRequest subjectUpdateRequest);

  /**
   * Delete method to delete a subject.
   *
   * @param id id of the subject to deactivate
   */
  void deleteSubject(String id);
}
