package com.ids.idstcitmtsmdtsubjectmanagement.util;

import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectCreateRequest;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectMutationResponse;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectQueryResponse;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectUpdateRequest;
import com.ids.idstcitmtsmdtsubjectmanagement.model.entity.Subject;

public interface SubjectAdapter {

  /**
   * Method to convert a SubjectCreateRequest to a Subject.
   *
   * @param subjectCreateRequest request body with dto to be parsed to Subject entity.
   * @return Subject
   */
  Subject toSubject(SubjectCreateRequest subjectCreateRequest);

  /**
   * Method to convert a SubjectUpdateRequest to a Subject.
   *
   * @param subject              Subject to be updated.
   * @param subjectUpdateRequest request body with dto to be parsed to Subject entity.
   * @return Subject
   */
  Subject toSubject(Subject subject, SubjectUpdateRequest subjectUpdateRequest);

  /**
   * Method to convert a Subject to a SubjectQueryResponse.
   *
   * @param subject Subject returned by the service.
   * @return SubjectQueryResponse
   */
  Subject toSubject(Integer status, Subject subject);

  /**
   * Method to convert a Subject to a SubjectQueryResponse.
   *
   * @param subject Subject returned by the service.
   * @return SubjectQueryResponse
   */
  SubjectQueryResponse toSubjectQueryResponse(Subject subject);

  /**
   * Method to convert a Subject to a SubjectMutationResponse.
   *
   * @param subject Subject returned by the service.
   * @return SubjectMutationResponse
   */
  SubjectMutationResponse toSubjectMutationResponse(Subject subject);
}
