package com.ids.idstcitmtsmdtsubjectmanagement.util;

/**
 * This class defines constants that represent the default values for properties related to the
 * Subject entity. (Esta clase define constantes que representan los valores por defecto para las
 * propiedades relacionadas con la entidad Subject.)
 */
public class SubjectDefaultValues {
  // Entity related constants
  /**
   * The default subject table name. (El nombre de tabla por defecto para Subject.)
   */
  public static final String SUBJECT_TABLE_NAME = "subject";

  /**
   * The default subject ID column name. (El nombre de columna por defecto para el ID de Subject.)
   */
  public static final String SUBJECT_ID_COLUMN = "id";

  /**
   * The default subject name column name. (El nombre de columna por defecto para el nombre de
   * Subject.)
   */
  public static final String SUBJECT_NAME_COLUMN = "name";

  /**
   * The default subject status column name. (El nombre de columna por defecto para el estado de
   * Subject.)
   */
  public static final String SUBJECT_STATUS_COLUMN = "status";

  /**
   * The default subject abbreviation column name. (El nombre de columna por defecto para la
   * abreviatura de Subject.)
   */
  public static final String SUBJECT_ABBREVIATION_COLUMN = "abbreviation";

  /**
   * The default subject hours column name. (El nombre de columna por defecto para las horas de
   * Subject.)
   */
  public static final String SUBJECT_HOURS_COLUMN = "hours";

  /**
   * The default subject schedule column name. (El nombre de columna por defecto para el horario de
   * Subject.)
   */
  public static final String SUBJECT_SCHEDULE_COLUMN = "schedule";

  /**
   * The default subject created by column name. (El nombre de columna por defecto para 'creado por'
   * de Subject.)
   */
  public static final String SUBJECT_CREATED_BY_COLUMN = "created_by";

  /**
   * The default subject created at column name. (El nombre de columna por defecto para 'creado en'
   * de Subject.)
   */
  public static final String SUBJECT_CREATED_AT_COLUMN = "created_at";

  /**
   * The default subject last modify by column name. (El nombre de columna por defecto para 'última
   * modificación por' de Subject.)
   */
  public static final String SUBJECT_LAST_MODIFY_BY_COLUMN = "last_modify_by";

  /**
   * The default subject last modify at column name. (El nombre de columna por defecto para 'última
   * modificación en' de Subject.)
   */
  public static final String SUBJECT_LAST_MODIFY_AT_COLUMN = "last_modify_at";

  // Service related constants
  public static final String SUBJECT_CREATED_BY = "API";
  public static final String SUBJECT_LAST_MODIFY_BY = "API";
  public static final int SUBJECT_STATUS_ACTIVE = 1;
  public static final int SUBJECT_STATUS_INACTIVE = 0;

  public static final String SUBJECT_NOT_FOUND = "Subject not found";
  public static final String SUBJECT_ID_ALREADY_EXISTS = "subjectId already exists";
  public static final String SUBJECT_ALREADY_EXISTS = "Subject already exists";
  public static final String SUBJECT_BLANK_VALUE = "Subject blank value";
}
