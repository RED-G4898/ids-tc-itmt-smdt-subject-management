package com.ids.idstcitmtsmdtsubjectmanagement.util;

/**
 * Default values for exceptions.
 */
public class ExceptionDefaultValues {

  /**
   * Invalid parameter exception default message.
   */
  public static final String INVALID_PARAMETER_EXCEPTION_MESSAGE = "invalidRequest";

  /**
   * Unauthorized exception default message.
   */
  public static final String UNAUTHORIZED_EXCEPTION_MESSAGE = "unAuthorized";

  /**
   * Access denied exception default message.
   */
  public static final String ACCESS_DENIED_EXCEPTION_MESSAGE = "accessNotConfigured";

  /**
   * Resource not found exception default message.
   */
  public static final String RESOURCE_NOT_FOUND_EXCEPTION_MESSAGE = "resourceNotFound";

  /**
   * Exception default message.
   */
  public static final String EXCEPTION_MESSAGE = "serverUnavailable";
}
