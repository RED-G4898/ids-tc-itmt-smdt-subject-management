package com.ids.idstcitmtsmdtsubjectmanagement.util;

import static com.ids.idstcitmtsmdtsubjectmanagement.util.SubjectDefaultValues.SUBJECT_CREATED_BY;
import static com.ids.idstcitmtsmdtsubjectmanagement.util.SubjectDefaultValues.SUBJECT_LAST_MODIFY_BY;
import static com.ids.idstcitmtsmdtsubjectmanagement.util.SubjectUtils.getNullPropertyNames;

import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectCreateRequest;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectMutationResponse;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectQueryResponse;
import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectUpdateRequest;
import com.ids.idstcitmtsmdtsubjectmanagement.model.entity.Subject;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Class to make conversions between Subject and Subject DTOs.
 */
@Component
public class SubjectAdapterImpl implements SubjectAdapter {

  /**
   * {@inheritDoc}.
   */
  @Override
  public Subject toSubject(SubjectCreateRequest subjectCreateRequest) {
    Subject subject = new Subject();
    BeanUtils.copyProperties(subjectCreateRequest, subject,
        getNullPropertyNames(subjectCreateRequest));
    subject.setSubjectId(UUID.randomUUID().toString());
    subject.setCreatedBy(SUBJECT_CREATED_BY);
    subject.setCreatedAt(LocalDateTime.now());
    return subject;
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public Subject toSubject(Subject subject, SubjectUpdateRequest subjectUpdateRequest) {
    BeanUtils.copyProperties(subjectUpdateRequest, subject,
        getNullPropertyNames(subjectUpdateRequest));
    subject.setLastModifyBy(SUBJECT_LAST_MODIFY_BY);
    subject.setLastModifyAt(LocalDateTime.now());
    return subject;
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public Subject toSubject(Integer status, Subject subject) {
    subject.setStatus(status);
    subject.setLastModifyBy(SUBJECT_LAST_MODIFY_BY);
    subject.setLastModifyAt(LocalDateTime.now());
    return subject;
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public SubjectQueryResponse toSubjectQueryResponse(Subject subject) {
    SubjectQueryResponse subjectQueryResponse = new SubjectQueryResponse();
    subjectQueryResponse.setSubjectId(subject.getSubjectId());
    subjectQueryResponse.setName(subject.getName());
    subjectQueryResponse.setStatus(subject.getStatus());
    subjectQueryResponse.setAbbreviation(subject.getAbbreviation());
    subjectQueryResponse.setHours(subject.getHours());
    subjectQueryResponse.setSchedule(subject.getSchedule());
    subjectQueryResponse.setCreatedBy(subject.getCreatedBy());
    subjectQueryResponse.setCreatedAt(subject.getCreatedAt().toString());
    subjectQueryResponse.setLastModifyBy(subject.getLastModifyBy());
    if (subject.getLastModifyAt() != null) {
      subjectQueryResponse.setLastModifyAt(subject.getLastModifyAt().toString());
    } else {
      subjectQueryResponse.setLastModifyAt(null);
    }
    return subjectQueryResponse;
  }

  /**
   * {@inheritDoc}.
   */
  @Override
  public SubjectMutationResponse toSubjectMutationResponse(Subject subject) {
    SubjectMutationResponse subjectMutationResponse = new SubjectMutationResponse();
    subjectMutationResponse.setSubjectId(subject.getSubjectId());
    return subjectMutationResponse;
  }
}
