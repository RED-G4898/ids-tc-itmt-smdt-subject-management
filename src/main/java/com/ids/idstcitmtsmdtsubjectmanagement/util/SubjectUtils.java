package com.ids.idstcitmtsmdtsubjectmanagement.util;

import com.ids.idstcitmtsmdtsubjectmanagement.model.dto.SubjectUpdateRequest;
import com.ids.idstcitmtsmdtsubjectmanagement.model.entity.Subject;
import com.ids.idstcitmtsmdtsubjectmanagement.model.repository.SubjectRepository;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * Class with general Subject utilities.
 */
@Component
public class SubjectUtils {

  private final SubjectRepository subjectRepository;

  public SubjectUtils(SubjectRepository subjectRepository) {
    this.subjectRepository = subjectRepository;
  }

  /**
   * Method to get the null property names of an object.
   *
   * @param source object to get the null property names
   * @return String[]
   */
  public static String[] getNullPropertyNames(Object source) {
    final BeanWrapper src = new BeanWrapperImpl(source);
    java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

    Set<String> emptyNames = new HashSet<>();
    for (java.beans.PropertyDescriptor pd : pds) {
      Object srcValue = src.getPropertyValue(pd.getName());
      if (srcValue == null) {
        emptyNames.add(pd.getName());
      }
    }
    String[] result = new String[emptyNames.size()];
    return emptyNames.toArray(result);
  }

  /**
   * Method to validate the creation of a subject.
   *
   * @param subject subject to validate
   */
  public void validateCreateSubject(Subject subject) {
    if (subjectRepository.findById(subject.getSubjectId()).isPresent()) {
      throw new IllegalArgumentException(SubjectDefaultValues.SUBJECT_ID_ALREADY_EXISTS);
    }

    if (subjectRepository.findByNameAndAbbreviationAndHoursAndSchedule(subject.getName(),
        subject.getAbbreviation(), subject.getHours(), subject.getSchedule()).isPresent()) {
      throw new IllegalArgumentException(SubjectDefaultValues.SUBJECT_ALREADY_EXISTS);
    }
  }

  /**
   * Method to validate the update of a subject.
   *
   * @param subjectUpdateRequest subject to validate
   */
  public void validateUpdateSubject(SubjectUpdateRequest subjectUpdateRequest) {
    isBlank(subjectUpdateRequest.getName(), subjectUpdateRequest.getAbbreviation(),
        subjectUpdateRequest.getSchedule());
  }

  /**
   * Method to validate if a String value es blank or only filled with white spaces.
   *
   * @param values Strings to validate
   */
  public void isBlank(String... values) {
    for (String value : values) {
      if (value != null && value.trim().isEmpty()) {
        throw new IllegalArgumentException(SubjectDefaultValues.SUBJECT_BLANK_VALUE);
      }
    }
  }
}
