package com.ids.idstcitmtsmdtsubjectmanagement.model.repository;

import com.ids.idstcitmtsmdtsubjectmanagement.model.entity.Subject;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SubjectRepository extends JpaRepository<Subject, String> {

  /**
   * Find a subject by name (Buscar una asignatura por nombre).
   *
   * @param name name of the subject to find
   * @return Subject
   */
  Optional<Subject> findByName(String name);

  /**
   * Find a subject by abbreviation (Buscar una asignatura por abreviatura).
   *
   * @param abbreviation abbreviation of the subject to find
   * @return Subject
   */
  Optional<Subject> findByAbbreviation(String abbreviation);

  Optional<Subject> findByNameAndAbbreviationAndHoursAndSchedule(String name, String abbreviation,
      Integer hours, String schedule);
}
