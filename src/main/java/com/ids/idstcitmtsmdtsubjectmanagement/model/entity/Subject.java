package com.ids.idstcitmtsmdtsubjectmanagement.model.entity;

import com.ids.idstcitmtsmdtsubjectmanagement.util.SubjectDefaultValues;

import lombok.Data;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class model/entity of subject table record in DB (Modelo de clase/entidad de los registros de la
 * tabla materias en la BD).
 */
@Data
@Entity
@Table(name = SubjectDefaultValues.SUBJECT_TABLE_NAME)
public class Subject {

  /**
   * Unique identifier and primary key of subject table (Identificador único y llave primaria de la
   * tabla de materia). Example (ejemplo): "001" | String
   */
  @Id
  @Column(name = SubjectDefaultValues.SUBJECT_ID_COLUMN, nullable = false)
  private String subjectId;

  /**
   * Whole name of a subject (Nombre entero de una asignatura). Example (ejemplo): "Mathematics" |
   * String
   */
  @Column(name = SubjectDefaultValues.SUBJECT_NAME_COLUMN)
  private String name;

  /**
   * Active or inactive status of a subject (Estado activo o inactivo de una asignatura). Example
   * (ejemplo): 1 | int
   */
  @Column(name = SubjectDefaultValues.SUBJECT_STATUS_COLUMN)
  private Integer status;

  /**
   * Short name form of a subject (Nombre corto de una asignatura). Example (ejemplo): "Mat" |
   * String(3)
   */
  @Column(name = SubjectDefaultValues.SUBJECT_ABBREVIATION_COLUMN, length = 3)
  private String abbreviation;

  /**
   * Hours duration of a subject (Duración en horas de una asignatura). Example (ejemplo): 5 | int
   */
  @Column(name = SubjectDefaultValues.SUBJECT_HOURS_COLUMN)
  private Integer hours;

  /**
   * Days assigned to a subject (Días en que se imparte una asignatura). Example (ejemplo): "L-V" |
   * String(13)
   */
  @Column(name = SubjectDefaultValues.SUBJECT_SCHEDULE_COLUMN, length = 13)
  private String schedule;

  /**
   * Origin identity of a subject creation (Identidad de origen de la creación de una asignatura).
   * Example (ejemplo): "API" "Employee 1" | String
   */
  @Column(name = SubjectDefaultValues.SUBJECT_CREATED_BY_COLUMN)
  private String createdBy;

  /**
   * Creation date of a subject (Fecha de creación de una materia). Example (ejemplo):
   * 2023-05-15T04:08:32 | Timestamp
   */
  @Column(name = SubjectDefaultValues.SUBJECT_CREATED_AT_COLUMN)
  private LocalDateTime createdAt;

  /**
   * Origin identity of a subject last modification (Identidad de origen de la fecha de última
   * modificación de una materia). Example (ejemplo): "API" "Employee 1" | String
   */
  @Column(name = SubjectDefaultValues.SUBJECT_LAST_MODIFY_BY_COLUMN)
  private String lastModifyBy;

  /**
   * Last modification date of a subject (Fecha de última modificación de una materia). Example
   * (ejemplo): 2023-05-15T04:08:32 | Timestamp
   */
  @Column(name = SubjectDefaultValues.SUBJECT_LAST_MODIFY_AT_COLUMN)
  private LocalDateTime lastModifyAt;
}
