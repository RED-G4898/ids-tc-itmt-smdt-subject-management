package com.ids.idstcitmtsmdtsubjectmanagement.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * DTO to set the structure of a subject mutation response to return just the id of the mutated
 * subject.
 */
@Data
public class SubjectMutationResponse {

  /**
   * Unique identifier of the subject (Identificador único de la asignatura). Example (ejemplo):
   * "a1b2c3d4e5f6g7h8i9j0k1l2m3n4o5p6" | String
   */
  @NotNull
  private String subjectId;
}
