package com.ids.idstcitmtsmdtsubjectmanagement.model.dto;

import lombok.Data;

@Data
public class SubjectUpdateRequest {

  /**
   * Whole name of a subject (Nombre entero de una asignatura). Example (ejemplo): "Mathematics" |
   * String
   */
  private String name;

  /**
   * Active or inactive status of a subject (Estado activo o inactivo de una asignatura). Example
   * (ejemplo): 1 | Integer
   */
  private Integer status;

  /**
   * Short name form of a subject (Nombre corto de una asignatura). Example (ejemplo): "Mat" |
   * String(3)
   */
  private String abbreviation;

  /**
   * Hours duration of a subject (Duración en horas de una asignatura). Example (ejemplo): 5 |
   * Integer
   */
  private Integer hours;

  /**
   * Days assigned to a subject (Días en que se imparte una asignatura). Example (ejemplo): "L-V" |
   * String(13)
   */
  private String schedule;
}
