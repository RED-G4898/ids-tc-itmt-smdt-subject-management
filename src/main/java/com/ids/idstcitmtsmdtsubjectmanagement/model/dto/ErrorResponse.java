package com.ids.idstcitmtsmdtsubjectmanagement.model.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * DTO to set the structure of an error response in order to simplify error reading by users.
 */
@Data
@Builder
public class ErrorResponse {

  /**
   * The type of error (El tipo de error). Example (ejemplo): "Bad Request" | String
   */
  private String type;
  /**
   * The code of error (El código de error). Example (ejemplo): "400" | String
   */
  private String code;
  /**
   * Error detailed info (Información detallada del error). Example (ejemplo): "Missing arguments" |
   * String
   */
  private String details;
  /**
   * The location of the error (La ubicación del error). Example (ejemplo):
   * "http://localhost:8080/subject" | String
   */
  private String location;
  /**
   * More info about the error (Más información sobre el error). Example (ejemplo):
   * "http://localhost:8080/subject" | String
   */
  private String moreInfo;
  /**
   * Unique identifier of the error (Identificador único del error). Example (ejemplo):
   * "a1b2c3d4e5f6g7h8i9j0k1l2m3n4o5p6" | String
   */
  private String uuid;
  /**
   * Timestamp of the error (Fecha y hora del error). Example (ejemplo): "2020-01-01T00:00:00 .000Z"
   * | String
   */
  private LocalDateTime timestamp;
}
