package com.ids.idstcitmtsmdtsubjectmanagement.model.dto;

import lombok.Data;

/**
 * DTO to set the structure of a subject query response to return all the information of a subject.
 */
@Data
public class SubjectQueryResponse {

  /**
   * Unique identifier of the subject (Identificador único de la asignatura). Example (ejemplo):
   * "a1b2c3d4e5f6g7h8i9j0k1l2m3n4o5p6" | String
   */
  private String subjectId;
  /**
   * Whole name of a subject (Nombre entero de una asignatura). Example (ejemplo): "Mathematics" |
   * String
   */
  private String name;
  /**
   * Active or inactive status of a subject (Estado activo o inactivo de una asignatura). Example
   * (ejemplo): 1 | Integer
   */
  private Integer status;
  /**
   * Short name form of a subject (Nombre corto de una asignatura). Example (ejemplo): "Mat" |
   * String(3)
   */
  private String abbreviation;
  /**
   * Hours duration of a subject (Duración en horas de una asignatura). Example (ejemplo): 5 |
   * Integer
   */
  private Integer hours;
  /**
   * Days assigned to a subject (Días en que se imparte una asignatura). Example (ejemplo): "L-V" |
   * String(13)
   */
  private String schedule;
  /**
   * User who created the subject (Usuario que creó la asignatura). Example (ejemplo):
   * "a1b2c3d4e5f6g7h8i9j0k1l2m3n4o5p6" | String
   */
  private String createdBy;
  /**
   * Date when the subject was created (Fecha en que se creó la asignatura). Example (ejemplo):
   * "2021-01-01T00:00:00.000Z" | String
   */
  private String createdAt;
  /**
   * User who last modified the subject (Usuario que modificó por última vez la asignatura). Example
   * (ejemplo): "a1b2c3d4e5f6g7h8i9j0k1l2m3n4o5p6" | String
   */
  private String lastModifyBy;
  /**
   * Date when the subject was last modified (Fecha en que se modificó por última vez la
   * asignatura). Example (ejemplo): "2021-01-01T00:00:00.000Z" | String
   */
  private String lastModifyAt;
}
